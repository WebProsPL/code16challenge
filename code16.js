(function(){
	const getKanyeResponse = async() => {
		const response = await fetch("https://api.kanye.rest/");
		if (!response.ok) return alert("something went wrong");
		try {
			const kanyeQuote = await response.json();
			alert(`Kanye says: ${kanyeQuote.quote}`);
			return kanyeQuote.quote;
		} catch (e) {
			return alert("something went wrong");
		}
	};
	const answer = prompt('Do you want to listen to Kanye? ("Y" if yes)');
	if (answer.toLowerCase() !== "y") return null;
	getKanyeResponse();
})();